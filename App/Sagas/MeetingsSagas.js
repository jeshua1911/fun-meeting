/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import moment from 'moment';
import { Alert } from 'react-native';
import { call, put } from 'redux-saga/effects'
import MeetingsActions from '../Redux/MeetingsRedux'
// import { MeetingsSelectors } from '../Redux/MeetingsRedux'

export function * getMeetings (api, action) {
  try {
    // get current data from Store
    // const currentData = yield select(MeetingsSelectors.getData)
    // make the call to the api
    const response = yield call(api.getmeetings)

    // success?
    if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(MeetingsActions.meetingsSuccess(response.data))
    } else {
      yield put(MeetingsActions.meetingsFailure())
      yield call(action.navigate, 'LoginScreen');
    }
  } catch(ex) {
    yield put(MeetingsActions.meetingsFailure())
    yield call(action.navigate, 'LoginScreen');
  }
}

export function * createMeeting (api, action) {
  let response = null;

  const { payload } = action;

  try {
    const data = {
      'name'     : encodeURIComponent( payload.name ),
      'agenda'   : encodeURIComponent( payload.agenda ),
      'schedule' : encodeURIComponent( moment( payload.schedule ).format() ),
      'url'      : encodeURIComponent( payload.url ),
      'location' : encodeURIComponent( payload.location ),
    };

    response = yield call(api.createmeeting, data);

    if ( response.data.data.error ) {
      yield put(MeetingsActions.meetingsCreateFailure( response.data.data ));
    } else {
      yield put(MeetingsActions.meetingsCreateSuccess( response.data ));
    }
  } catch ( error ) {
    yield put(MeetingsActions.meetingsCreateFailure( response.data.data ));
  }
}

export function * createMeetingReset ( action ) {
  yield call( action.navigation.goBack );
  yield put( MeetingsActions.meetingsRequest( action.navigation.navigate ) );
}
