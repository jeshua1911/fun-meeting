import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
      backgroundColor: 'rgba(0,0,0,.87)',
      flex: 1
  }
})
