import React, { Component } from 'react'
import { View, ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'

// Styles
import styles from './Styles/MainScreenStyle'

import MainHeader from '../Components/MainHeader';
import MyMeetings from '../Components/MyMeetings';
import AttendMeetings from '../Components/AttendMeetings';
import MyTasks from '../Components/MyTasks';

class MainScreen extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      'current_screen' : 'my_meetings'
    };
  }

  renderCurrentScreen () {
    if ( this.state.current_screen === 'my_meetings' ) {
      return <MyMeetings navigate={ this.props.navigation.navigate } />
    } else if ( this.state.current_screen === 'attend_meetings' ) {
      return <AttendMeetings />
    } else {
      return <MyTasks />;
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={ styles.mainContent }>
          { this.renderCurrentScreen() }
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
