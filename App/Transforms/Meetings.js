export function validateCreateMeetingData ( data ) {
  let response = {
    error: false,
    message : {}
  };

  if ( data.name === '' ) {
    response.error = true;
    response.message.name = [
      'The name field is required.'
    ];
  }
  if ( data.agenda === '' ) {
    response.error = true;
    response.message.agenda = [
      'The agenda field is required.'
    ];
  }
  if ( data.location === '' ) {
    response.error = true;
    response.message.location = [
      'The location field is required.'
    ];
  }
  if ( data.url === '' ) {
    response.error = true;
    response.message.url = [
      'The url field is required.'
    ];
  }

  return response;
}
