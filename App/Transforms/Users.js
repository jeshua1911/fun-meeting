export function filterUsers ( text, list ) {
  if ( text === '' ) {
    return [];
  }

  const regexp = new RegExp( text, 'i' );

  return list.filter( ( user ) => {
    return regexp.test( user.Name );
  } );
}

export function attendeeAlreadyAdded ( user, attendees ) {
  const index = attendees.findIndex( ( attendee ) => {
    if ( attendee.UserId === user.UserId ) {
      return true;
    }

    return false;
  } );

  return index !== -1;
}
