import React, { PureComponent } from 'react'
import moment from 'moment';
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity, CheckBox } from 'react-native'
import styles from './Styles/MeetingItemStyle'

import CalendarDateIcon from './CalendarDateIcon';

import { Images } from '../Themes';

export const CheckBoxSection = ( props ) => {
  if ( props.mode !== 'selection' )
    return null;

  return <View style={ styles.checkboxContainer }>
    <CheckBox onValueChange={ ( value ) => props.checkboxValueChanged( props.data, value ) } value={ props.data.selected === true } />
  </View>;
}

class MeetingItem extends PureComponent {
  render () {
    const props = this.props;
    const date = moment( props.data.schedule );

    return (
      <TouchableOpacity onPress={ this.props.onPress } onLongPress={ this.props.onLongPress }>
        <View style={styles.container}>
          <CalendarDateIcon month={ date.format( 'MMMM' ) } day={ date.format( 'DD' ) } />
          <View style={ styles.detailsContainer }>
            <Text style={ styles.titleText }>{ props.data.name }</Text>
            <Text style={ styles.agendaText }>Agenda: { props.data.agenda }</Text>
            <View style={ styles.locationWrapper }>
              <Text style={ styles.locationText }>Location: { props.data.location }</Text>
            </View>
            <Text style={ styles.locationText }>{ props.data.time }</Text>
          </View>
          <CheckBoxSection checkboxValueChanged={ props.checkboxValueChanged } mode={ props.mode } data={ props.data} />
        </View>
      </TouchableOpacity>
    );
  }
};

MeetingItem.propTypes = {
  data : PropTypes.object,
  mode : PropTypes.string,
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
};

export default MeetingItem;
