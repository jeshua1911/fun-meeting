import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View } from "react-native";
import styles from "./Styles/MeetingViewAttendeesStyle";
import {
  Container,
  Content,
  CardItem,
  Card,
  Body,
  Text,
  Left,
  Right, 
  Icon
} from "native-base";

const MeetingViewAttendees = props => {
  const { attendees } = props;
  return (
    <View style={styles.container}>
          <CardItem>
            <Body>
              <Text style={styles.subTitleText}>Attendees</Text>
              {attendees.map(attendee => (
                <CardItem>
                  <Left>
                    <Text>{attendee.Name}</Text>
                  </Left>
                  <Right>
                    <Icon name="close" />
                  </Right>
                </CardItem>
              ))}
            </Body>
          </CardItem>
    </View>
  );
};

export default MeetingViewAttendees;
