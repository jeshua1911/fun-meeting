import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Alert, View, Text, ActivityIndicator, TouchableOpacity } from 'react-native'
import { Button, Icon, Container, Header, Content, Card, CardItem, Body } from 'native-base';
import styles from './Styles/SavingModalStyle'

import MeetingsCreators from '../Redux/MeetingsRedux';

class SavingModal extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      meeting_create_status: props.meeting_create_status,
      meeting_create_response: props.meeting_create_response
    };
  }

  componentWillReceiveProps ( nextProps ) {
    let state = {};

    if ( this.state.meeting_create_status !== nextProps.meeting_create_status ) {
        state.meeting_create_status = nextProps.meeting_create_status;
    }

    if ( this.state.meeting_create_response !== nextProps.meeting_create_response ) {
        state.meeting_create_response = nextProps.meeting_create_response;
    }

    this.setState( state );
  }

  render () {
    const { meeting_create_response, meeting_create_status } = this.state;

    if ( [ null, 'failed' ].indexOf( meeting_create_status ) !== -1 ) {
      return null;
    }

    if ( [ 'done' ].indexOf( meeting_create_status ) !== -1 && meeting_create_response.error ) {
      return null;
    }

    if ( meeting_create_status === 'done'
        && meeting_create_response !== null
        && !meeting_create_response.error ) {
      return (
        <View style={styles.container}>
          <View style={ styles.boxContainer }>
            <View style={ styles.messageContainer }>
              <Icon name="checkmark" family="FontAwesome" style={{ color: 'green' }} />
              <Text style={ styles.messageStyle }>Meeting successfully saved!</Text>
            </View>
            <View style={ styles.linkButtonContainer }>
              <TouchableOpacity onPress={ () => this.props.createMeetingReset( this.props.navigation ) }>
                <Text style={ styles.continueStyle }>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        );
    }

    return (
      <View style={styles.container}>
        <View style={ styles.boxContainer }>
          <ActivityIndicator size="small" color="green" />
          <Text style={ styles.messageStyle }>Saving meeting, please wait..</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = ( state ) => {
  return {
    meeting_create_status: state.meetings.meeting_create_status,
    meeting_create_response: state.meetings.meeting_create_response
  };
};

const mapDispatchToProps = ( dispatch ) => {
  return {
    createMeetingReset ( navigation ) {
      dispatch( MeetingsCreators.meetingsCreateRequestReset( navigation ) );
    }
  };
}

SavingModal.propTypes = {
  navigation: PropTypes.object
};

export default connect( mapStateToProps, mapDispatchToProps ) ( SavingModal );
