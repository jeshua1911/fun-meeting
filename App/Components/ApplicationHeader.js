import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Alert, View, Text } from 'react-native'
import { Drawer, Button, Icon, Container, Title, Header, Body, Left, Right } from 'native-base';
import styles from './Styles/ApplicationHeaderStyle'

import DrawerCreators from '../Redux/DrawerRedux';

class ApplicationHeader extends Component {
  renderButtonLeft = () => {
    const { routeName } = this.props;

    if ( routeName !== 'MyMeetings' ) {
      return <Left style={ styles.menuContainer }>
        <Button transparent onPress={ () => this.props.goBack() }>
          <Icon style={{ color: '#fff' }} name="arrow-back" />
        </Button>
      </Left>
    } else {
      return <Left style={ styles.menuContainer }>
        <Button transparent onPress={ () => this.props.toggleDrawer() }>
          <Icon style={{ color: '#fff' }} name="menu" />
        </Button>
      </Left>
    }
  }

  render () {
    return (
      <Container style={styles.container}>
        <Header style={styles.headerStyle}>
        { this.renderButtonLeft() }
          <Body style={styles.funmeetingContainer}>
            <Title style={styles.funMeetingText}>Fun Meeting</Title>
          </Body>
        </Header>
      </Container>
    )
  }
}

const mapStateToProps = ( state ) => {
  return {};
};

const mapDispatchToProps = ( dispatch ) => {
  return {
    toggleDrawer () {
      dispatch( DrawerCreators.toggleDrawer() );
    }
  };
};

export default connect( mapStateToProps, mapDispatchToProps ) ( ApplicationHeader );
