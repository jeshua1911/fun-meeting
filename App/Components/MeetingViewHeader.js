import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/MeetingViewHeaderStyle'
import { Icon, Content } from 'native-base';
import 'native-base/Fonts/FontAwesome.ttf';

import { Images } from '../Themes'

const MeetingViewHeader = ( props ) => {
  return <View style={styles.container}>
    <TouchableOpacity onPress={ () => props.goBack() }>
      <View>
        <Image source={ Images.backButton } />
      </View>
    </TouchableOpacity>
    <Text style={ styles.titleStyle }>{ props.data.name }</Text>
  </View>;
};

MeetingViewHeader.propTypes = {
  data : PropTypes.object
};

export default MeetingViewHeader;
