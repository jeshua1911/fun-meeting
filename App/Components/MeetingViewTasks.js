import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View } from "react-native";
import styles from "./Styles/MeetingViewTasksStyle";
import { Container, Content, Card, CardItem,  ListItem,  CheckBox,  Body, Text} from "native-base";

export default class MeetingViewTasks extends Component {

  render() {
    return (
      <View style={styles.container}>
          <Content>
            <Card>
              <CardItem header>
              <Text style={styles.subTitleText}>TASKS</Text>
              </CardItem>
              <ListItem>
                <CheckBox checked={true} />
                <Body>
                  <Text>Daily Stand Up</Text>
                </Body>
              </ListItem>
              <ListItem>
                <CheckBox checked={false} />
                <Body>
                  <Text>Discussion with Client</Text>
                </Body>
              </ListItem>
              <ListItem>
                <CheckBox checked={false} />
                <Body>
                  <Text>Discussion with Client</Text>
                </Body>
              </ListItem>
            </Card>
          </Content>
      </View>
    );
  }
}
