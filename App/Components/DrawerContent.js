import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from './Styles/DrawerContentStyle'
import { Card, CardItem, Icon, Left }from 'native-base';
import Images from '../Themes/Images';

const User = ( props ) => {
  return <View style={ styles.userContainer }>
    <Image style={ styles.backgroundImage } resizeMode="stretch" source={ Images.headerBackground } />
    <Text style={ styles.userName }>Edmondo Abuda</Text>
    <Text style={ styles.userEmail }>edmondoa@codev.com</Text>
  </View>;
};

const DrawerContent = ( props ) => {
  const { navigate, signOut } = props;

  return <View style={styles.container}>
    <User />
    <Card>
      <TouchableOpacity style={ styles.touchable } onPress={ () => navigate( 'MyMeetings' ) }>
        <CardItem>
            <Icon style={ styles.iconStyle } name="home" color="grey" />
            <Text style={ styles.itemTextStyle }>My Meetings</Text>
        </CardItem>
      </TouchableOpacity>
      <TouchableOpacity style={ styles.touchable } onPress={ () => navigate( 'AttendMeetings' ) }>
        <CardItem>
            <Icon style={ styles.iconStyle } name="flower" color="grey" />
            <Text style={ styles.itemTextStyle }>Meetings I Created</Text>
        </CardItem>
      </TouchableOpacity>
      <TouchableOpacity style={ styles.touchable } onPress={ () => navigate( 'MyTasks' ) }>
        <CardItem>
            <Icon style={ styles.iconStyle } name="hammer" color="grey" />
            <Text style={ styles.itemTextStyle }>My Tasks</Text>
        </CardItem>
      </TouchableOpacity>
      <TouchableOpacity style={ styles.touchable } onPress={ () => signOut() }>
        <CardItem>
            <Icon style={ styles.iconStyle } name="log-out" color="grey" />
            <Text style={ styles.itemTextStyle }>Logout</Text>
        </CardItem>
      </TouchableOpacity>
    </Card>
  </View>;
}

DrawerContent.propTypes = {
  navigate: PropTypes.func
};

export default DrawerContent;
