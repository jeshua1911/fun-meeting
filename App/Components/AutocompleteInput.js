import React, { Component } from 'react'
import _ from 'lodash';
import PropTypes from 'prop-types';
import { View, TextInput, ScrollView, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { Item, Label } from 'native-base';
import styles from './Styles/AutocompleteInputStyle'

const ResultItem = ( { person, onPress } ) => {
  return <TouchableOpacity onPress={ () => onPress( person ) }>
    <View style={ styles.resultItemContainer }>
      <Text>{ person.Name }</Text>
    </View>
  </TouchableOpacity>;
};

export default class AutocompleteInput extends Component {
  constructor ( props ) {
    super( props );

    this.state = {
      focused: false,
      results: props.results || [],
      search_text: props.searchText || ''
    };

    this._search = false;
  }

  componentWillReceiveProps ( nextProps ) {
    if ( !_.isEqual( nextProps.results, this.state.results ) ) {
      this.setState( { 'results' : nextProps.results } );
    }

    if ( this.state.search_text !== nextProps.searchText ) {
      this.setState( { 'search_text' : nextProps.searchText } )
    }
  }

  itemPressed = ( item ) => {
    this.props.onPersonSelected( item );
    this.setState( { focused: false } );
    this._search.blur();
  }

  renderResults = () => {
    if ( this.state.focused && this.state.results.length ) {
      return <View style={ styles.resultsContainer }>
        <ScrollView keyboardShouldPersistTaps="always" style={{flex: 1}} contentContainerStyle={ { flex: 1 } }>
          { this.state.results.map( ( person, index ) => <ResultItem onPress={ this.itemPressed } key={ `result-item-${ index }` } person={ person } /> ) }
        </ScrollView>
      </View>
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Text>Attendees:</Text>
        <Item>
          <Label>Search</Label>
          <TextInput
            underlineColorAndroid="transparent"
            style={ styles.inputStyle }
            ref={ ( c ) => this._search = c }
            value={ this.state.search_text }
            onChangeText={ this.props.searchTextChanged }
            onFocus={ () => this.setState( { focused: true } ) }
            onBlur={ () => this.setState( { focused: false } ) } />
        </Item>
        { this.renderResults() }
      </View>
    )
  }
}

AutocompleteInput.propTypes = {
  results: PropTypes.array,
  searchText: PropTypes.string,
  onPersonSelected: PropTypes.func,
};

AutocompleteInput.defaultProps = {
  results: [],
};
