import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 5,
    paddingLeft: 5,
  },
  datepickerContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  formStyle: {
    paddingLeft: 0,
    paddingRight: 0,
    marginLeft: 0,
    marginRight: 0,
  },
  datepicker: {
    width: 250,
  },
  itemStyle: {
    height: 70,
    marginLeft: 0,
    marginRight: 0,
  },
  errorMessageContainer: {
    height: 20,
    justifyContent: 'center',
  },
  errorMessageStyle: {
    fontSize: 10,
    lineHeight: 20,
    color: 'red',
  },
  labelStyle: {
    marginTop: 5,
  },
  memberListContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingLeft: 50,
    paddingRight: 50,
    minHeight: 250,
  },
  buttonContainer: {
    marginRight: 5,
    marginBottom: 5,
  }
})
