import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold'
  },
  tableContainerStyle: {
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.2)',
    flexDirection: 'row',
    flex: 1,
  },
  tableStyle: {
    flexDirection: 'column',
    flex: 1,
  },
  pageTitleContainer: {
    alignItems: 'center',
    marginBottom: 10
  }
})
