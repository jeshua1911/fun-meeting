import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
    backgroundColor: 'rgba(0, 0, 0, 0.43)'
  },
  boxContainer: {
    minWidth: 200,
    minHeight: 150,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#ddd',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  messageContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  messageStyle: {
    marginLeft: 10,
  },
  continueStyle: {
    color: '#388E3C',
  },
  linkButtonContainer: {
    height: 20,
    alignItems: 'flex-end',
  },
})
