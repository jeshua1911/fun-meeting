import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  touchable: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: 15,
  },
  itemTextStyle: {
    fontSize: 14,
    fontFamily: 'Roboto',
  },
  userContainer: {
    position: 'relative',
    height: 50,
    justifyContent: 'center',
    paddingLeft: 30,
    backgroundColor: 'blue',
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  userName: {
    fontFamily: 'Roboto',
    fontSize: 20,
    color: '#fff',
  },
  userEmail: {
    color: '#D81B60',
  },
})
