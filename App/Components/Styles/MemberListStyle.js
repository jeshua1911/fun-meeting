import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  memberListItemContainer: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
  },
  personLogoContainer: {
    width: 40,
  },
  personName: {
    flex: 1,
  },
  remove: {
    width: 40,
  }
})
