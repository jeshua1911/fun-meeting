import { StyleSheet } from 'react-native'
import {Fonts} from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
      
  subTitleText: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: Fonts.type.base,
  },  

        
  underlinedText: {
    fontSize: 15,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    fontFamily: Fonts.type.base,
  },  
})
