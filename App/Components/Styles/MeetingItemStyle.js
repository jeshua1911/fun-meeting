import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.25)',
  },
  detailsContainer: {
    flex: 1,
    paddingLeft: 20,
  },
  titleText: {
    fontWeight: 'bold'
  },
  agendaText: {
    fontSize: 10
  },
  locationWrapper: {
    flexDirection: 'row',
  },
  locationText: {
    fontSize: 10,
  },
  showMapText: {
    textDecorationLine: 'underline',
    color: 'blue',
    marginLeft: 2,
    fontSize: 9,
  },
  mapImage: {
    width: 15,
    height: 15,
    marginLeft: 5,
  },
  checkboxContainer: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  }
})
