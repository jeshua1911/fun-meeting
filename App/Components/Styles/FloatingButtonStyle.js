import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 50,
    height: 50,
    alignItems: 'stretch',
    justifyContent: 'center',
    borderRadius: 50,
    marginRight: 10,
    marginBottom: 10,
  },
  touchable: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  }
})
