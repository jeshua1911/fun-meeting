import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'rgb(0, 188, 212)',
    backgroundColor: '#039BE5',
  },
  tabButton: {
    flex: 1,
    height: 40,
    alignItems: 'stretch',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderRightColor: 'rgba(0,0,0,.2)',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,.2)',
  },
  iconButtonStyle: {
    width: 25,
    height: 25,
  },
  touchable: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
