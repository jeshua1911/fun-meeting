import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native'
import styles from './Styles/AppHeaderStyle'

import { Images } from '../Themes'

const AppHeader = ( props ) => {
  return <View style={ styles.container }>
    <Image source={ Images.headerBackground } style={ styles.headerBackgroundStyle } resizeMode='cover' />
    <View style={ styles.profilePictureContainer }>
      <Image source={ props.profileImage } style={ styles.profilePicture } resizeMode='contain' />
    </View>
    <View style={ styles.rightItemsContainer }>
      <Text style={ styles.nameText }>Jeshua Ensong</Text>
    </View>
  </View>;
};

export default AppHeader;
