import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, FlatList } from 'react-native'
import styles from './Styles/MeetingListStyle'

import MeetingItem from './MeetingItem';

const MeetingList = ( props ) => {
  return <View style={styles.container}>
    <View style={ styles.tableContainerStyle }>
      <FlatList
        data={ props.list }
        extraData={ props.list.filter( ( item ) => item.selected === true ) }
        style={ styles.tableStyle }
        keyExtractor={ ( item, index ) => `meeting-item-${ index }` }
        renderItem={ ( meeting ) => <MeetingItem
          checkboxValueChanged={ props.onCheckboxValueChanged }
          mode={ props.mode }
          onPress={ () => props.onItemPressed( meeting.item ) }
          onLongPress={ () => props.onItemLongPressed( meeting.item ) }
          data={ meeting.item } /> }
        />
    </View>
  </View>;
}

MeetingList.propTypes = {
  list                   : PropTypes.array,
  mode                   : PropTypes.string,
  onItemPressed          : PropTypes.func,
  onItemLongPressed      : PropTypes.func,
  onCheckboxValueChanged : PropTypes.func,
};

export default MeetingList;
