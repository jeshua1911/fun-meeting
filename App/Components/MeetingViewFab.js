import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { Text } from "react-native";
import styles from "./Styles/MeetingViewFabStyle";
import { Container, Header, View, Button, Icon, Fab } from "native-base";

const MeetingViewFab = props => {
  // const { agenda, createdByName } = props;
  const container = {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 100,
    height: 100,
    alignItems: 'stretch',
    justifyContent: 'center',
    borderRadius: 50,
    marginRight: 10,
    marginBottom: 10,
  };
  this.state = {
    active: "true"
  };
  return (
      <View style={container}>
        <Fab>
          <Icon name="add" />
        </Fab>
      </View>
  );
};

export default MeetingViewFab;
