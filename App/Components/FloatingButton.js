import React, { Component } from 'react'
import Immutable from 'seamless-immutable';
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/FloatingButtonStyle'
import { Icon } from 'native-base';

const FloatingButton = ( props ) => {
  const container = {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 50,
    height: 50,
    alignItems: 'stretch',
    justifyContent: 'center',
    borderRadius: 50,
    marginRight: 10,
    marginBottom: 10,
  };

  container.backgroundColor = props.color;

  return <View style={ container }>
    <TouchableOpacity onPress={ props.onPress } style={ styles.touchable }>
        <Icon name={ props.icon } family="FontAwesome" style={{ 'color': '#fff'}} />
    </TouchableOpacity>
  </View>;
}

FloatingButton.defaultProps = {
  color: '#d50000'
};

export default FloatingButton;
