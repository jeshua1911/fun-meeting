import { createReducer, createActions } from 'reduxsauce'
import { Alert } from 'react-native';
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  toggleDrawer: null,
  closeDrawer: null,
})

export const DrawerTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  toggle: 'hide'
})

/* ------------- Selectors ------------- */

export const DrawerSelectors = {
  getData: state => state.toggle
}

/* ------------- Reducers ------------- */

export const updateToggle = (state, action) => {
  const toggle = state.toggle == 'show' ? 'hide' : 'show';

  return state.merge({ toggle })
}
export const closeDrawer = (state) =>
  state.merge({ toggle: 'close' })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TOGGLE_DRAWER]: updateToggle,
  [Types.CLOSE_DRAWER]: closeDrawer,
})
