import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  usersRequest: ['data'],
  usersSuccess: ['payload'],
  usersFailure: null
})

export const UsersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: { data: [] },
  error: null
})

/* ------------- Selectors ------------- */

export const UsersSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: { data: [] } })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: 'done', error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: 'done', error: true, payload: { data: [] } })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USERS_REQUEST]: request,
  [Types.USERS_SUCCESS]: success,
  [Types.USERS_FAILURE]: failure
})
