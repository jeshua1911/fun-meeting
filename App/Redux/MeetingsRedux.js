import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  meetingsRequest: ['navigate'],
  meetingsSuccess: ['payload'],
  meetingsFailure: null,
  meetingsCreateRequest: [ 'payload' ],
  meetingsCreateSuccess: [ 'response' ],
  meetingsCreateFailure: [ 'response' ],
  meetingsCreateRequestReset: [ 'navigation' ],
})

export const MeetingsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: { data: [] },
  error: null,
  meeting_create_status: null,
  meeting_create_response: { message: {} },
})

/* ------------- Selectors ------------- */

export const MeetingsSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: 'done', error: null, payload })
};

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: 'done', error: true, payload: { data: [] } });

export const createMeeting = ( state ) => {
  return state.merge( { meeting_create_status: 'processing' } );
};

export const createMeetingSuccess = (state, { response }) => {
  return state.merge( { 'meeting_create_status': 'done', 'meeting_create_response': response } );
}

export const createMeetingFailure = ( state, { response } ) => {
  return state.merge( { 'meeting_create_status': 'failed', 'meeting_create_response' : response } );
}

export const createMeetingReset = state => 
  state.merge({ meeting_create_status: null, meeting_create_response: { message: {} } });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MEETINGS_REQUEST]: request,
  [Types.MEETINGS_SUCCESS]: success,
  [Types.MEETINGS_FAILURE]: failure,
  [Types.MEETINGS_CREATE_REQUEST]: createMeeting,
  [Types.MEETINGS_CREATE_REQUEST_RESET]: createMeetingReset,
  [Types.MEETINGS_CREATE_SUCCESS]: createMeetingSuccess,
  [Types.MEETINGS_CREATE_FAILURE]: createMeetingFailure,
})
