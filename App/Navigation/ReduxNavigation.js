import React from 'react'
import { BackHandler, Alert } from 'react-native'
import { addNavigationHelpers, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import AppNavigation from './AppNavigation'

class ReduxNavigation extends React.Component {
  componentDidMount () {
    BackHandler.addEventListener( 'hardwareBackPress', this.onBackPress );
  }

  componentWillUnmount () {
    BackHandler.removeEventListener( 'hardwareBackPress', this.onBackPress );
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;

    if ( nav.index > 1 ) {
      dispatch( NavigationActions.back() );

      return true;
    }

    return false;
  }

  render () {
    const { dispatch, nav } = this.props;
    const navigation = addNavigationHelpers({
      dispatch,
      state: nav
    });

    return (
      <AppNavigation navigation={navigation} />
    );
  }
}

const mapStateToProps = state => ({ nav: state.nav })
export default connect(mapStateToProps)(ReduxNavigation)
