import { StackNavigator } from 'react-navigation'
import AddMeetingScreen from '../Containers/AddMeetingScreen'
import MeetingViewScreen from '../Containers/MeetingViewScreen'
import LoginScreen from '../Containers/LoginScreen'
import MainScreen from '../Containers/MainScreen'

import MyMeetings from '../Components/MyMeetings';
import AttendMeetings from '../Components/AttendMeetings';
import MyTasks from '../Components/MyTasks';

import React from 'react';
import MeetingViewHeader from '../Components/MeetingViewHeader';
import ApplicationHeader from '../Components/ApplicationHeader';

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LoginScreen: { screen: LoginScreen },
}, {
  // Default config for all screens
  headerMode: 'none', //float
  initialRouteName: 'LoginScreen',
});

const SecondaryNav = StackNavigator( {
    Root: {
      screen: PrimaryNav,
      navigationOptions: {
        header: null
      }
    },
    MainScreen: { screen: MainScreen },
    AttendMeetings: { screen: AttendMeetings },
    MyMeetings: { screen: MyMeetings },
    MyTasks: { screen: MyTasks },
    AddMeetingScreen: { screen: AddMeetingScreen },
    MeetingViewScreen: { screen: MeetingViewScreen },
  },
  {
    headerMode: 'float', //float
    navigationOptions: ( { navigation } ) => ( {
      header: <ApplicationHeader routeName={ navigation.state.routeName } goBack={ navigation.goBack } />,
      headerStyle: styles
    } )
} );

export default SecondaryNav;
